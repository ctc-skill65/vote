<?php

return [
    'app_name' => 'ระบบลงคะแนนเลือกตั้งออนไลน์',
    'site_url' => 'http://skill65.local/vote',

    'db_host' => 'localhost',
    'db_user' => 'root',
    'db_password' => 'root',
    'db_name' => 'skill65_vote',
    'db_charset' => 'utf8mb4'
];